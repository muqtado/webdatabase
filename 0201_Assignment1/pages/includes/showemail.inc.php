            <style>
        .msg-alert{
            display: block;
            height: 15px;
            width: 15px;
            line-height: 15px;

            -webkit-border-radius: 50%;
            border-radius: 50%;

            background-color: grey;
            color: white;
            text-align: center;
            font-size: 4px; 
        }

    </style>
            
            <ul class="nav navbar-top-links navbar-right" ng-controller="customerCtrl2" ng-init="customerCtrl2.initialize()">
            
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" ng-click="clearEmail()" style="    display: inline-flex;">
                        <i class="fa fa-envelope fa-fw"></i><i ng-if = "count != 0" class="msg-alert">{{count}}</i>  <i class="fa fa-caret-down"> </i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages" >
                        <li ng-repeat="x in myData">
                            <a href="#">
                                <div>
                                    <strong>{{x.sender}}</strong>
                                    <span class="pull-right text-muted">
                                        <em>{{x.timestamp}}</em>
                                    </span>
                                </div>
                                <div>{{x.msgdetail}}</div>
                            </a>    
                            <div class='divider'/>                          
                        </li>                                       
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->