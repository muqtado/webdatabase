
var app = angular.module('myApp', []);
app.controller('customerCtrl2', function($scope, $http){
    $http.get("unreadmsg.php").then(function(respond){
        $scope.myData = respond.data;
        $scope.count = respond.data.length;     
    });   

    $scope.clearEmail = function(){
        $scope.count = 0;

        $.ajax({
            type: "POST",
            url: "editemail.php"
        }); 
    }

}); 
app.controller('personsjson', function($scope, $http){
    console.log(1);
    $http.get("personsjson.php").then(function(respond){
        $scope.myData2 = respond.data; 
        console.log($scope.myData2);   
    });   
}); 

app.controller('cardinfojson', function($scope, $http){
    $http.get("cardinfojson.php").then(function(respond){
        $scope.myData3 = respond.data;   
        console.log($scope.myData3);    
     });  
});  

/*app.controller('xmltransactions', function($scope, $http){
    $http.get("transactions.php",
       {
            transformResponse: function(cnv){
                var x2js = new X2JS();
                var aftCnv = x2js.xml_str2json(cnv);
                return aftCnv;
            }
       })
    .success(function(response){
        $scope.details = response.response.transactions.transaction;
        console.log(response.response.transactions.transaction);
    });   

});  */


  app.controller('xmltransactions', function($scope, $http){
    $http.get("transactions.php").then(function(respond){ 
        console.log(respond);
        var arr = [];
        var parser = new DOMParser ();
        var xmlDoc = parser.parseFromString(respond.data, 'text/xml');
        console.log(xmlDoc);
        var transaction = xmlDoc.getElementsByTagName('transaction');

            for (var i = 0 ; i < transaction.length; i++)
            {
                var transvalue = transaction[i].getElementsByTagName('transno')[0].innerHTML;
                var uidvalue = transaction[i].getElementsByTagName('uid')[0].innerHTML;
                var datvalue = transaction[i].getElementsByTagName('date')[0].innerHTML;
                var sellervalue = transaction[i].getElementsByTagName('sellerno')[0].innerHTML;
                var productvalue = transaction[i].getElementsByTagName('product')[0].innerHTML;
                var pricevalue = transaction[i].getElementsByTagName('price')[0].innerHTML;
                var numbervalue = transaction[i].getElementsByTagName('number')[0].innerHTML;           
                arr.push({
                    transno: transvalue, uid: uidvalue, date: datvalue, 
                    sellerno: sellervalue, product: productvalue, 
                    price: pricevalue, number: numbervalue});
                

            }
            $scope.myData5 = arr; 
            console.log($scope.myData5);     
        });
});

